import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Welcome to CDK deployed app</h1>
        <h2> This a React App is hosted in Amazon S3 Bucket</h2>
      </header>
    </div>
  );
}

export default App;
